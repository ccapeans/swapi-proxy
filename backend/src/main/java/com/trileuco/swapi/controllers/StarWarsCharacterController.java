package com.trileuco.swapi.controllers;

import com.trileuco.swapi.models.Character;
import com.trileuco.swapi.helpers.SwapiURLHelper;
import org.json.*;
import com.trileuco.swapi.helpers.RequestHelper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/person-info")
public class StarWarsCharacterController {

    @GetMapping("/")
    @Cacheable("character")
    public Character getCharacter(@RequestParam(value = "name") String name) {
        try {
            return this.fetchCharacter(name);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    @GetMapping("/all")
    @Cacheable("characters")
    public ArrayList<Character> getAllCharacters(
            @RequestParam(value = "page", defaultValue = "1") int pageNumber
    ) {

        try {
            return this.fetchCharacters(pageNumber);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Character fetchCharacter(String characterName) throws IOException {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("search", characterName);

        String response = new RequestHelper(SwapiURLHelper.baseUrl, parameters).doRequest();
        JSONObject jsonCharacter = (JSONObject) retrieveCharactersOfResponse(response).get(0);

        if (!jsonCharacter.isEmpty()) {
            return this.transformJsonToCharacter(jsonCharacter);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    private ArrayList<Character> fetchCharacters(int pageNumber) throws IOException {
        ArrayList<Character> characters = new ArrayList<Character>();

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("page", Integer.toString(pageNumber));

        String response = new RequestHelper(SwapiURLHelper.baseUrl, null).doRequest();
        JSONArray jsonCharacters = retrieveCharactersOfResponse(response);

        if (!jsonCharacters.isEmpty()) {
            for (Object jsonCharacter : jsonCharacters) {
                Character character = this.transformJsonToCharacter((JSONObject) jsonCharacter);
                characters.add(character);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return characters;
    }

    private static JSONArray retrieveCharactersOfResponse(String response) {
        return new JSONObject(response).getJSONArray("results");
    }

    private Character transformJsonToCharacter(JSONObject jsonObject) throws IOException {
        return Character.FromJSONObject(jsonObject);
    }

}
