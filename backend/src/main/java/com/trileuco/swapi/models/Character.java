package com.trileuco.swapi.models;

import com.trileuco.swapi.helpers.VehicleSelectorHelper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Character {
    private String name;
    private String birthYear;
    private String gender;
    private Planet planetName;
    private Vehicle fastestVehicleDriven;
    private List<Film> films;


    public static Character FromJSONObject (JSONObject obj){
        try {
            return new Character(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param obj this is a json object that contains the response of the web request given by SWAPI
     * @throws IOException
     */
    public Character(JSONObject obj) throws IOException {
        assignObjectDataToAttributes(obj);
    }

    private void assignObjectDataToAttributes(JSONObject obj) throws IOException {
        this.name = obj.getString("name");
        this.birthYear = obj.getString("birth_year");
        this.gender = obj.getString("gender");
        this.gender = obj.getString("gender");
        this.planetName = Planet.FromURL(obj.getString("homeworld"));
        this.films = parseFilmsData(obj.getJSONArray("films"));
        this.fastestVehicleDriven = VehicleSelectorHelper.getFastest(
                parseVehiclesAndStarshipsData(
                        obj.getJSONArray("vehicles"),
                        obj.getJSONArray("starships"))
        );
    }

    private static ArrayList<Film> parseFilmsData(JSONArray jsonFilms) throws IOException {
        ArrayList<Film> films = new ArrayList();
        Iterator<Object> filmsIterator = jsonFilms.iterator();
        while (filmsIterator.hasNext()) {
            Film auxFilm = Film.FromURL(filmsIterator.next().toString());
            films.add(auxFilm);
        }
        return films;
    }

    private static ArrayList<Vehicle> parseVehiclesAndStarshipsData(
            JSONArray jsonVehicles,
            JSONArray jsonStarships
    ) throws IOException {

        ArrayList<Vehicle> vehicles = new ArrayList();
        List<Object> vehiclesAndStarshipsArray = joinVehiclesAndStarshipsArray(jsonVehicles, jsonStarships);

        if (!vehiclesAndStarshipsArray.isEmpty()) {
            for (Object vehicle : vehiclesAndStarshipsArray) {
                Vehicle auxVehicle = Vehicle.FromURL(vehicle.toString());
                vehicles.add(auxVehicle);
            }
        }
        return vehicles;
    }

    private static List<Object> joinVehiclesAndStarshipsArray(JSONArray jsonVehicles, JSONArray jsonStarships) {
        return Stream.concat(
                jsonVehicles.toList().stream(),
                jsonStarships.toList().stream()
        ).collect(Collectors.toList());
    }

    public String getName() {
        return name;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public String getGender() {
        return gender;
    }

    public String getPlanetName() {
        return planetName.getName();
    }

    public String getFastestVehicleDriven() {
        if (fastestVehicleDriven != null) {
            return fastestVehicleDriven.getName();
        }
        return "This character hasn't driven any vehicles";
    }

    public List<Film> getFilms() {
        return this.films;
    }
}
