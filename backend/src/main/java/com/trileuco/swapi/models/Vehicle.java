package com.trileuco.swapi.models;

import com.trileuco.swapi.helpers.RequestHelper;
import org.json.JSONObject;

import java.io.IOException;

public class Vehicle {
    private String name;
    private Integer maxSpeed;

    public static Vehicle FromURL(String url){
        try {
            return new Vehicle(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Vehicle(String vehicleUrl) throws IOException {
        RequestHelper rq = new RequestHelper(vehicleUrl, null);
        String response = rq.doRequest();
        JSONObject obj = new JSONObject(response);
        this.name = obj.getString("name");
        this.maxSpeed = obj.getInt("max_atmosphering_speed");
    }

    public String getName() {
        return name;
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }
}
