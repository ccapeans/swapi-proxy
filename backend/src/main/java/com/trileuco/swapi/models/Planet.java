package com.trileuco.swapi.models;

import com.trileuco.swapi.helpers.RequestHelper;
import org.json.JSONObject;

import java.io.IOException;

public class Planet {
    private String name;
    private String population;
    private Integer rotationPeriod;
    // The rest of the attributes if needed would be added here and in the constructor

    public static Planet FromURL(String url){
        try {
            return new Planet(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Planet(String name, String population, Integer rotationPeriod) {
        this.name = name;
        this.population = population;
        this.rotationPeriod = rotationPeriod;
    }

    public Planet(String planetUrl) throws IOException {
        RequestHelper rq = new RequestHelper(planetUrl, null);
        String response = rq.doRequest();
        JSONObject obj = new JSONObject(response);
        this.name = obj.getString("name");
        // The rest of the attributes, if needed, would be added here in the same way that "name"
    }


    public String getName() {
        return name;
    }
}
