package com.trileuco.swapi.models;

import com.trileuco.swapi.helpers.RequestHelper;
import org.json.JSONObject;

import java.io.IOException;

public class Film {
    private String name;
    private String releaseDate;

    public static Film FromURL(String url){
        try {
            return new Film(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Film(String name, String releaseDate) {
        this.name = name;
        this.releaseDate = releaseDate;
    }

    public Film(String filmUrl) throws IOException {
        RequestHelper rq = new RequestHelper(filmUrl, null);
        String response = rq.doRequest();
        JSONObject obj = new JSONObject(response);
        this.name = obj.getString("title");
        this.releaseDate = obj.getString("release_date");

    }

    public String getName() {
        return name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @Override
    public boolean equals(Object obj) {
        Film comparisonFilm = (Film) obj;
        return this.name == comparisonFilm.name && this.releaseDate == comparisonFilm.releaseDate;
    }
}
