package com.trileuco.swapi.helpers;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;

public class RequestHelper {

    private URL url;

    public RequestHelper(
            String url,
            Map<String, String> parameters
    ) throws MalformedURLException, UnsupportedEncodingException {
        this.url = parameters == null ?
                new URL(url)
                :
                new URL(url + ParameterStringBuilder.convertParamsMapToString(parameters));
    }

    public String doRequest() throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        setConnectionSettings(con);

        if (didRequestFail(con)) {
            String error = getErrorString(con);
            System.out.println(error);
            return error;
        }

        String responseContent = getResponseContent(con);

        // Finally we close the connection
        con.disconnect();

        return responseContent;
    }

    private static void setConnectionSettings(HttpURLConnection con) throws ProtocolException {
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
    }

    private static boolean didRequestFail(HttpURLConnection con) throws IOException {
        // Now we read the response of the request
        int status = con.getResponseCode();
        return status > 299;
    }

    private static String getErrorString(HttpURLConnection con) throws IOException {
        Reader streamReader = new InputStreamReader(con.getErrorStream());
        BufferedReader reader = new BufferedReader(streamReader);
        StringBuilder sb = new StringBuilder();
        String str;
        while ((str = reader.readLine()) != null) {
            sb.append(str);
        }
        return "Error: " + sb.toString();
    }

    private static String getResponseContent(HttpURLConnection con) throws IOException {
        String inputLine;
        StringBuilder content = new StringBuilder();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream())
        );
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        return content.toString();
    }
}
