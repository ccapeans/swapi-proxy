package com.trileuco.swapi.helpers;

public class SwapiURLHelper {
    public final static String port = "1138";
    public final static String baseUrl = "http://swapi.trileuco.com" + (!port.isEmpty() ? (":" + port) : "") + "/api/people/";
}
