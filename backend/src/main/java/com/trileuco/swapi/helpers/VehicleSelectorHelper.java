package com.trileuco.swapi.helpers;

import com.trileuco.swapi.models.Vehicle;

import java.util.ArrayList;

public class VehicleSelectorHelper {

    public static Vehicle getFastest(ArrayList<Vehicle> vehicles){
        if(vehicles.size() < 1){
            return null;
        }
        Vehicle fastest = vehicles.get(0);
        for(Vehicle vehicle : vehicles){
            if(fastest.getMaxSpeed() < vehicle.getMaxSpeed()){
                fastest = vehicle;
            }
        }
        return fastest;
    }
}
