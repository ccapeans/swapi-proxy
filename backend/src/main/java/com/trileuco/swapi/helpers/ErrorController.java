package com.trileuco.swapi.helpers;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController {
    @RequestMapping("/error")
    @ResponseBody
    public String handleError(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("status_code", statusCode);
        jsonResponse.put("message", exception);
        if(statusCode == 404){
            jsonResponse.put("message", "The character wasn't found. Try again.");
        }

        return jsonResponse.toString();
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
