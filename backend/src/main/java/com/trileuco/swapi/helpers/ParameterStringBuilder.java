package com.trileuco.swapi.helpers;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class ParameterStringBuilder {
    public static String convertParamsMapToString(Map<String, String> params)
            throws UnsupportedEncodingException {

        String joinedParameters = joinParams(params);
        return correctFormatInParameters(joinedParameters);
    }

    private static String joinParams(Map<String, String> params) throws UnsupportedEncodingException {
        StringBuilder joinedParameters = new StringBuilder();
        joinedParameters.append("?");

        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                joinedParameters.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                joinedParameters.append("=");
                joinedParameters.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
                joinedParameters.append("&");
            }
        }
        return joinedParameters.toString();
    }

    private static String correctFormatInParameters(String joinedParameters) {
        return
                joinedParameters.length() > 0 ?
                        joinedParameters.substring(0, joinedParameters.length() - 1)
                        :
                        joinedParameters;
    }
}