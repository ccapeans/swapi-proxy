package com.trileuco.swapi;


import com.trileuco.swapi.controllers.StarWarsCharacterController;
import com.trileuco.swapi.models.Character;
import com.trileuco.swapi.models.Film;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

class JsonCharacterExample {
    public static JSONObject characterJson = new JSONObject("{\n" +
            "  \"name\": \"Luke Skywalker\",\n" +
            "  \"height\": \"172\",\n" +
            "  \"mass\": \"77\",\n" +
            "  \"hair_color\": \"blond\",\n" +
            "  \"skin_color\": \"fair\",\n" +
            "  \"eye_color\": \"blue\",\n" +
            "  \"birth_year\": \"19BBY\",\n" +
            "  \"gender\": \"male\",\n" +
            "  \"homeworld\": \"http://swapi.trileuco.com:1138/api/planets/1/\",\n" +
            "  \"films\": [\n" +
            "    \"http://swapi.trileuco.com:1138/api/films/1/\",\n" +
            "    \"http://swapi.trileuco.com:1138/api/films/2/\",\n" +
            "    \"http://swapi.trileuco.com:1138/api/films/3/\",\n" +
            "    \"http://swapi.trileuco.com:1138/api/films/6/\"\n" +
            "  ],\n" +
            "  \"species\": [],\n" +
            "  \"vehicles\": [\n" +
            "    \"http://swapi.trileuco.com:1138/api/vehicles/14/\",\n" +
            "    \"http://swapi.trileuco.com:1138/api/vehicles/30/\"\n" +
            "  ],\n" +
            "  \"starships\": [\n" +
            "    \"http://swapi.trileuco.com:1138/api/starships/12/\",\n" +
            "    \"http://swapi.trileuco.com:1138/api/starships/22/\"\n" +
            "  ],\n" +
            "  \"created\": \"2014-12-09T13:50:51.644000Z\",\n" +
            "  \"edited\": \"2014-12-20T21:17:56.891000Z\",\n" +
            "  \"url\": \"http://swapi.trileuco.com:1138/api/people/1/\"\n" +
            "}");
}

@SpringBootTest
@AutoConfigureMockMvc
class SwapiApplicationTests {

    @Autowired
    private StarWarsCharacterController controller;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    void getSingleCharacter() throws Exception {
        Character SWCharacter = Character.FromJSONObject(JsonCharacterExample.characterJson);
        Character APISWCharacter = controller.getCharacter("Luke Skywalker");
        assertThat(SWCharacter.getName().equals(APISWCharacter.getName()));
        assertThat(SWCharacter.getBirthYear().equals(APISWCharacter.getBirthYear()));
        assertThat(SWCharacter.getFastestVehicleDriven().equals(APISWCharacter.getFastestVehicleDriven()));
    }

    @Test
    void getSingleCharacterFilms() throws Exception {
        Character SWCharacter = Character.FromJSONObject(JsonCharacterExample.characterJson);
        Character APISWCharacter = controller.getCharacter("Luke Skywalker");
        for(Film film : SWCharacter.getFilms()){
            assertThat(APISWCharacter.getFilms().contains(film));
        }
    }

    @Test
    void getAllCharacters() throws Exception {
        ArrayList<Character> characters = controller.getAllCharacters(1);
        assertThat(characters.size() == 82);

    }
}
