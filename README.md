# DOCUMENTATION

This is a swapi proxy server made with React.js and Spring Boot. 

![Frontend image at start](sample_images/frontend-image.png)
## Steps to use it:

1. <code>docker-compose up --build</code>
2. Go to localhost:3000 to see the client
3. Go to localhost:8080 to see the backend

## How to open it if you don't have Docker

### Start the backend

* #### With JAR:  
    <i>Steps 1 and 2 are optional, the code is already compiled in a JAR file.</i>

    1. Open a terminal in PATH_TO_PROJECT/backend
    2. Write <code>./gradlew build</code>
    3. Look for a folder named <b>build</b> inside backend.
    4. Double click in the JAR file and that should start the backend in <b>localhost:8080</b>

* #### Executing the code:

    1. Open a terminal in PATH_TO_PROJECT/backend
    2. Write <code>./gradlew bootRun</code>
    3. The server should start running in <b>localhost:8080</b>



### Start the frontend

1. Open a terminal in PATH_TO_PROJECT/frontend
2. Write <code>npm install</code>
3. Write <code>npm start</code>
4. The frontend should be launched in <b>localhost:3000</b>