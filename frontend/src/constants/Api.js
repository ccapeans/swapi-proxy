export const baseUrl = "http://localhost:8080/swapi-proxy";
export const characterBaseUrl = baseUrl + "/person-info";
export const allCharactersUrl = characterBaseUrl + "/all";