import React, {useEffect, useState} from 'react';
import {Container} from "semantic-ui-react";
import Header from "./components/Header";
import Content from "./components/Content";

function App() {
    const [searchText, setSearchText ] = useState("");

    useEffect(() => {

    }, [searchText])
    return (
        <Container className="app-container">
            <Header {...{setSearchText}} />
            <Content {...{searchText}} />
        </Container>
    );
}

export default App;
