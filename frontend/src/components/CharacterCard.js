import React from "react";
import {Feed, List, Card} from "semantic-ui-react";

import "./CharacterCard.css";

const CharacterCard = ({name, birthYear, gender, planetName, fastestVehicleDriven, films}) => {
    return (
        <Card>
            <Card.Content>
                <Card.Header>{name}</Card.Header>
                <Card.Description>
                    <List>
                        <List.Item icon={gender === "male" ? "male" : "female"} content={gender}/>
                        <List.Item icon='birthday' content={birthYear}/>
                        <List.Item icon='space shuttle' content={fastestVehicleDriven}/>
                        <List.Item icon='marker' content={planetName}/>
                    </List>
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Card>
                    <Card.Content>
                        <Card.Header>Appeared in...</Card.Header>
                    </Card.Content>
                    <Card.Content className="film-card">
                        <Feed>
                            {films && films.map((film, i) => (
                                <Feed.Event key={i}>
                                    <Feed.Content className="single-film">
                                        <Feed.Summary>
                                            {i + 1}) {film.name} in {film.releaseDate}
                                        </Feed.Summary>
                                    </Feed.Content>
                                </Feed.Event>
                            ))}
                        </Feed>
                    </Card.Content>
                </Card>
            </Card.Content>
        </Card>
    )
}

export default CharacterCard;