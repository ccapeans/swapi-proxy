import React from "react";


const NotFund = () => (
    <h1>
        Nothing was found, Try again with another name.
    </h1>
)

export default NotFund;