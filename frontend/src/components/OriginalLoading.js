import React from "react";
import "./OriginalLoading.css";

export default () => (
    <div className="body">
        <div className="canvas">
            <div className="scrolling-area">
                <div className="rabbit-main rabbit-floor rabbit-jump">
                    <div className="rabbit-body-part rabbit-body"></div>
                    <div className="rabbit-body-part rabbit-tail"></div>
                    <div className="rabbit-body-part rabbit-foot">
                        <div className="foot-cover"></div>
                    </div>
                    <div className="rabbit-body-part rabbit-head"></div>
                    <div className="rabbit-body-part rabbit-ear  rabbit-ear-1">
                        <div className="ear-cover"></div>
                    </div>
                    <div className="rabbit-body-part rabbit-ear  rabbit-ear-2">
                        <div className="ear-cover"></div>
                    </div>
                </div>
                <div className="egg-outer egg-1">
                    <div className="egg-line egg-line-1"></div>
                    <div className="egg-line egg-line-2"></div>
                    <div className="egg-line egg-line-3"></div>
                    <div className="egg-line egg-line-4"></div>
                    <div className="egg-line egg-line-5"></div>
                </div>
                <div className="egg-outer egg-2">
                    <div className="egg-line egg-line-1"></div>
                    <div className="egg-line egg-line-2"></div>
                    <div className="egg-line egg-line-3"></div>
                    <div className="egg-line egg-line-4"></div>
                    <div className="egg-line egg-line-5"></div>
                </div>
                <div className="egg-outer egg-3">
                    <div className="egg-line egg-line-1"></div>
                    <div className="egg-line egg-line-2"></div>
                    <div className="egg-line egg-line-3"></div>
                    <div className="egg-line egg-line-4"></div>
                    <div className="egg-line egg-line-5"></div>
                </div>
                <div className="egg-outer egg-4">
                    <div className="egg-line egg-line-1"></div>
                    <div className="egg-line egg-line-2"></div>
                    <div className="egg-line egg-line-3"></div>
                    <div className="egg-line egg-line-4"></div>
                    <div className="egg-line egg-line-5"></div>
                </div>
            </div>
            <div className="ground">
            </div>
        </div>
        <div className="loading" id="loading">
            <h1>LOADING</h1>
        </div>
    </div>
)