import React, {useState} from "react";
import {Input} from 'semantic-ui-react';
import "./Header.css";

const Header = ({setSearchText}) => {

    const [typingTimeout, setTypingTimeout] = useState(false);

    const onSearch = (event, {value}) => {
        if(typingTimeout){
            clearTimeout(typingTimeout);
        }
        setTypingTimeout(setTimeout(() => setSearchText(value), 1000));
    }

    return(
        <Input fluid className="search-box" onChange={onSearch} placeholder="Try with 'Luke Skywalker'" />
    )
}

export default Header;
