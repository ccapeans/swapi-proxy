import React, {useEffect, useState} from "react";
import {Grid, Pagination, Container} from 'semantic-ui-react';
import "./Content.css";
import CharacterCard from "./CharacterCard";
import {CHARACTERS_PER_PAGE, NUMBER_OF_CHARACTERS} from "../constants/data";
import NotFound from "./NotFound";
import {requestAllCharacters, requestCharacterByName} from "../api/requestCharacter";
import OriginalLoading from "./OriginalLoading";
import axios from "axios";

const Content = ({searchText}) => {
    const [characters, setCharacters] = useState([]);
    const [pageNumber, setPageNumber] = useState(1);
    const [isLoading, setIsLoading] = useState(true);


    useEffect(() => {
        const source = axios.CancelToken.source();

        async function fetchData() {
            // We need to pass the page number in order to retrieve the characters
            const charactersData = await requestAllCharacters(pageNumber, source);
            setCharacters(charactersData, setIsLoading(false));
        }

        async function fetchSearchedCharacter() {
            const charactersData = await requestCharacterByName(searchText, source);
            setCharacters(charactersData, setIsLoading(false));
        }

        setIsLoading(true);

        if (searchText) {
            fetchSearchedCharacter();
        } else {
            fetchData();
        }

    }, [pageNumber, searchText]);

    const onPageChange = (event, data) => {
        const {activePage} = data;
        console.log("Event: ", event);
        console.log("data: ", data);
        setPageNumber(activePage);
        window.scrollTo(0, 0);
    }


    return (
        <React.Fragment>
            {
                (isLoading) ?
                    <OriginalLoading/>
                    :
                    (
                        <React.Fragment>
                            {
                                (characters && characters.length > 0) ?
                                    (
                                        <React.Fragment>
                                            <div className="container">
                                                <Grid doubling columns={5}>
                                                    {characters.map((character, i) => (
                                                        <Grid.Column key={i} width={3}>
                                                            <CharacterCard {...character}/>
                                                        </Grid.Column>
                                                    ))}

                                                </Grid>
                                            </div>
                                            {
                                                characters.length >= CHARACTERS_PER_PAGE ?
                                                    <Container fluid>
                                                        <Pagination
                                                            boundaryRange={1}
                                                            defaultActivePage={pageNumber}
                                                            siblingRange={1}
                                                            totalPages={Math.ceil(NUMBER_OF_CHARACTERS / CHARACTERS_PER_PAGE)}
                                                            onPageChange={onPageChange}
                                                        />
                                                    </Container>
                                                    :
                                                    ""
                                            }

                                        </React.Fragment>
                                    )
                                    :
                                    <React.Fragment>
                                        {
                                            isLoading ? "" : <NotFound/>
                                        }
                                    </React.Fragment>


                            }
                        </React.Fragment>
                    )
            }
        </React.Fragment>
    )
}

export default Content;

