import axios from 'axios';
import {allCharactersUrl, characterBaseUrl} from "../constants/Api";
import axiosCancel from 'axios-cancel';

axiosCancel(axios, {
    debug: false // default
});

const requestId = "allCharacters";


export const requestCharacterByName = async (characterName, source) => {
    //source.cancel("Request not needed anymore");
    axios.cancelAll();

    console.log("Entra aqui y se cancela")
    if (characterName.length >= 1) {
        const response = await axios.get(
            characterBaseUrl + `/?name=${characterName}`
        )
            .catch(thrown => {
                console.log("There was an error in the request: requestCharacterByName");
            });
        ;

        if (!response || response.status >= 299) {
            return [];
        }
        return [response.data];
    }

}

export const requestAllCharacters = async (pageNumber, source) => {
    try {
        const response = await axios.get(
            allCharactersUrl + `/?page=${pageNumber}`,
            {
                requestId
            }
        )

        if (!response || response.status >= 299) {
            return [];
        }
        return response.data;

    } catch (err) {
        console.log("There was an error or the request was cancelled");
    }
    ;


}